package security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import security.model.Role;
import security.model.RoleName;
import security.repository.RoleRepository;

@Service
public class RoleService {

    @Autowired
    private RoleRepository repository;

    public void saveRole(Role role){
        repository.save(role);
    }

    public Role getRoleByName(RoleName roleName){
        return repository.findByRoleName(roleName);
    }
}
