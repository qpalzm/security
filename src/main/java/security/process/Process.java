package security.process;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import security.model.Role;
import security.model.RoleName;
import security.model.User;
import security.service.RoleService;
import security.service.UserService;

@Component
public class Process {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @EventListener(ApplicationReadyEvent.class)
    private void runFirst(){
        System.out.println("run security.process.Process.runFirst");
        User userFromDB = userService.getUserByLogin("admin");

        if (userFromDB != null) {
            return ;
        }

        Role adminRole = new Role();
        adminRole.setRoleName(RoleName.ROLE_ADMIN);

        User admin = new User();
        admin.setPassword("admin");
        admin.setLogin("admin");

        Role userRole = new Role();
        userRole.setRoleName(RoleName.ROLE_USER);

        User test = new User();
        test.setLogin("test");
        test.setPassword("test");

        adminRole.setUsers(Collections.singleton(admin));
        HashSet<User> userHashSet = new HashSet<>();
        userHashSet.add(admin);
        userHashSet.add(test);
        userRole.setUsers(userHashSet);

        admin.setRoles(Arrays.asList(adminRole, userRole));
        test.setRoles(Collections.singletonList(userRole));

        roleService.saveRole(adminRole);
        roleService.saveRole(userRole);
        userService.saveUserWithRole(admin);
        userService.saveUserWithRole(test);
    }



}
