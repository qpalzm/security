package security.model;

public enum RoleName {
    ROLE_ADMIN("ADMIN"),
    ROLE_USER("USER");

    private String value;
    RoleName(String name) {
        this.value = name;
    }

    public String getValue() {
        return value;
    }
}
