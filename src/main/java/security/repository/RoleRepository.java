package security.repository;

import security.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import security.model.RoleName;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByRoleName(RoleName roleName);

}
